package com.linuxiao.fullscreenwallpaper;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.linuxiao.widget.DBActionBarTabViewPage;
import com.linuxiao.widget.PagerSlidingTabStrip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by xiongxiaobin on 14-7-12.
 */
public class MainFrament extends Fragment implements ViewPager.OnPageChangeListener, ActionBar.TabListener {

    private   static MainFrament mainFrament;
    private ArrayList<HashMap<String, Object>> mDatas;

    public static MainFrament newInstance() {
        if (mainFrament == null) {

            mainFrament = new MainFrament();
        }
        return  mainFrament;
    }

    private ViewPager mViewPager;
    private Context mContext;
    private ViewPagerAdapter viewPagerAdapter;
    private PagerSlidingTabStrip tabs;


    private List<DBActionBarTabViewPage> mTabs;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        findViewById(rootView);
        initView();
        Log.e("tag","MainFramgent onCreateView");
        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getActivity();
        mDatas = new ArrayList<HashMap<String, Object>>();
        String[] titles = new String[]{"我", "推荐", "排行榜", "分类"};
        Display display = getActivity().getWindowManager().getDefaultDisplay();

        Object[] obj = new Object[]{HomeFragment.newInstance(), TuijianFragment.newInstance("" + display.getWidth() + "x" + display.getHeight(), 0), TuijianFragment.newInstance("x", (int) Math.random() * 100), FenLeiFragment.newInstance()};
        for (int i = 0; i < 4; i++) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("fragment", obj[i]);
            map.put("title", titles[i]);
            mDatas.add(map);
        }


        Log.e("tag","MainFramgent onCreate");
    }



    private void findViewById(View rootView) {
        mViewPager = (ViewPager) rootView.findViewById(R.id.pager);
        tabs = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);


    }

    private void initView() {
        initData();
        mViewPager.setAdapter(viewPagerAdapter);
        mViewPager.setOnPageChangeListener(this);

        tabs.setViewPager(mViewPager);

    }

    private void initData() {

        viewPagerAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager(), mDatas);
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        // TODO Auto-generated method stub
        Log.e("tag","onPageScrolled" + arg0);
    }

    @Override
    public void onPageSelected(int arg0) {
        //mActionBar.setSelectedNavigationItem(arg0);
        Log.e("tag","onPageSelected" + arg0);

    }

    @Override
    public void onTabReselected(ActionBar.Tab arg0, FragmentTransaction arg1) {
        // TODO Auto-generated method stub
        Log.e("tag","onTabReselected" + arg0);

    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction arg1) {
        mViewPager.setCurrentItem(tab.getPosition());
        Log.e("tag","onTabReselected" + tab);

    }

    @Override
    public void onTabUnselected(ActionBar.Tab arg0, FragmentTransaction arg1) {
        // TODO Auto-generated method stub

    }

}
