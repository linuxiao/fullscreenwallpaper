# README #

This README would normally document whatever steps are necessary to get your application up and running.

### 什么是全屏壁纸 ###

* 全屏壁纸为了解决现有壁纸软件，设置壁纸需要裁剪，拉升变形等问题。
* Version 1.0


### 已经实现功能  ###

* 1、我的收藏
* 2、推荐。推荐功能会根据手机的分辨率推荐一些该分辨率下的壁纸。
* 3、排行榜
* 4、分类。分类会根据不同的分辨率浏览图片
* 5、关于
* 6、设置清空缓存
* 7、瀑布流详细页面，设置壁纸。

### 待实现功能和修复功能 ###

* 文件浏览
* 壁纸大小重新调整
* 收藏中滑动ViewPage设置图片BUG

### 联系作者 ###

* QQ：894936258
* Email：linuxiao@gmail.com

### 全屏壁纸项目截图 ###

![device-2014-08-13-220917.gif](https://bitbucket.org/repo/bAz5jn/images/1739185676-device-2014-08-13-220917.gif)
