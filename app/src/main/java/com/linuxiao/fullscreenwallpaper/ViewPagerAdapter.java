package com.linuxiao.fullscreenwallpaper;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;
import java.util.List;

/**
 * Created by xiongxiaobin on 14-7-12.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private List<HashMap<String, Object>> mDatas;

    public ViewPagerAdapter(FragmentManager fm, List<HashMap<String, Object>> mDatas) {
        super(fm);
        this.mDatas = mDatas;
    }

    /**
     * Return the Fragment associated with a specified position.
     *
     * @param position
     */
    @Override
    public Fragment getItem(int position) {
        return (Fragment) mDatas.get(position).get("fragment");
    }

    /**
     * Return the number of views available.
     */
    @Override
    public int getCount() {
        return mDatas.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mDatas.get(position).get("title").toString();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
//        super.destroyItem(container, position, object);
    }
}
