package com.linuxiao.fullscreenwallpaper;



import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FuliFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class FuliFragment extends Fragment {


    public static FuliFragment newInstance() {
        FuliFragment fragment = new FuliFragment();
        return fragment;
    }
    public FuliFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fuli, container, false);
    }


}
