package com.linuxiao.fullscreenwallpaper;



import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.lurencun.android.adapter.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FenLeiFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class FenLeiFragment extends Fragment implements AdapterView.OnItemClickListener {


    private GridView gridView;
    private ArrayList<HashMap<String, String>> list;

    public static FenLeiFragment newInstance() {
        FenLeiFragment fragment = new FenLeiFragment();

        return fragment;
    }
    public FenLeiFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fen_lei, container, false);
        findViews(rootView);
        return  rootView;
    }

    private void findViews(View rootView) {
        gridView =(GridView)rootView.findViewById(R.id.gridView);
        gridView.setOnItemClickListener(this);
        list = new ArrayList<HashMap<String, String>>();
        for (int i = 0; i < SqlDataHelper.tmps.length; i++) {
            HashMap<String,String> map = new HashMap<String, String>();
            map.put("fbl",SqlDataHelper.tmps[i]);
            list.add(map);
        }
        gridView.setAdapter(new android.widget.SimpleAdapter(getActivity(),list,R.layout.item_text,new String[]{"fbl"},new int[]{R.id.textView}));
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(getActivity(),TuiJianActivity.class);
        intent.putExtra("fbl",SqlDataHelper.tmps[i]);
        startActivity(intent);

    }
}
