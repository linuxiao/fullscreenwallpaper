package com.linuxiao.fullscreenwallpaper;



import android.os.Bundle;

import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.linuxiao.fullscreenwallpaper.util.FileSizeUtil;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SettingFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class SettingFragment extends Fragment {

    private TextView tv;
    private File file;

    public static SettingFragment newInstance() {
        SettingFragment fragment = new SettingFragment();

        return fragment;
    }
    public SettingFragment() {
        // Required empty public constructor
    }
    Handler handler = new Handler(){
        @Override
        public void dispatchMessage(Message msg) {
            switch (msg.what){
                case 1:
                    showCacheSize();
                    break;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_setting, container, false);
        tv= (TextView) rootView.findViewById(R.id.textView);
        file =ImageLoader.getInstance().getDiskCache().getDirectory();
        showCacheSize();
        Button btnClear = (Button)rootView.findViewById(R.id.btnClear);
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageLoader.getInstance().getDiskCache().clear();
                handler.sendEmptyMessage(1);
            }
        });
        return  rootView;
    }
    void showCacheSize(){
        tv.setText("缓存文件夹为：" +file.getAbsolutePath() +"\n缓存大小为："+ FileSizeUtil.getAutoFileOrFilesSize(file.getAbsolutePath()));

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (hidden == false){
            showCacheSize();
        }
    }
}
