package com.linuxiao.fullscreenwallpaper;

import uk.co.senab.photoview.PhotoView;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.HackyViewPager;
import com.viewpagerindicator.PageIndicator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;


public class ImagePagerActivity extends FragmentActivity implements ViewPager.OnPageChangeListener {

    protected ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options;
	private static final String STATE_POSITION = "STATE_POSITION";

	private static final String IMAGES = "images";

	private static final String IMAGE_POSITION = "image_index";
    ArrayList<ImageDB> list = new ArrayList<ImageDB>();
    private int curIndex = 0;
	

	HackyViewPager pager;
	PageIndicator mIndicator;
    private AlertDialog dialog;


    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ac_image_pager);
        ((LXApplication)getApplication()).addActivity(this);
        getActionBar().setDisplayHomeAsUpEnabled(true);
		initImageLoader(this);
        Intent i = getIntent();
        Bundle b = i.getExtras();
        list = (ArrayList<ImageDB>) b.getSerializable("data");
        curIndex = b.getInt("index") - 1;
//
//		Bundle bundle = getIntent().getExtras();
//		String[] imageUrls = bundle.getStringArray(IMAGES);
//		int pagerPosition = bundle.getInt(IMAGE_POSITION, 0);

		if (savedInstanceState != null) {
			curIndex = savedInstanceState.getInt(STATE_POSITION);
		}

		options = new DisplayImageOptions.Builder()
			.showImageForEmptyUri(R.drawable.ic_empty)
			.showImageOnFail(R.drawable.ic_error)
			.resetViewBeforeLoading(true)
			.cacheOnDisc(true)
			.imageScaleType(ImageScaleType.EXACTLY)
			.bitmapConfig(Bitmap.Config.ARGB_8888)
			.displayer(new FadeInBitmapDisplayer(300))
			.build();

		pager = (HackyViewPager) findViewById(R.id.pager);
		pager.setAdapter(new ImagePagerAdapter(list,this));
		pager.setCurrentItem(curIndex);
        pager.setOnPageChangeListener(this);
		 mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
	     mIndicator.setViewPager(pager);
        dialog = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        dialog.setView(inflater.inflate(R.layout.progress_dialog,null,false));
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);

	}

	public  void initImageLoader(Context context) {
		// This configuration tuning is custom. You can tune every option, you may tune some of them,
		// or you can create default configuration by
		//  ImageLoaderConfiguration.createDefault(this);
		// method.
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
				.threadPriority(Thread.NORM_PRIORITY - 2)
				.denyCacheImageMultipleSizesInMemory()
				.discCacheFileNameGenerator(new Md5FileNameGenerator())
				.tasksProcessingOrder(QueueProcessingType.LIFO)
				.writeDebugLogs() // Remove for release app
				.build();
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

            getMenuInflater().inflate(R.menu.image_detail, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id){
            case R.id.set_wallpaper:
                setMyWallpaper();
                break;
            case R.id.set_skin:
                setMySkin();
                break;
            case R.id.save:
                saveImage();
                break;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveImage() {
        dialog.show();
        new Thread(){
            @Override
            public void run() {
                try {

                    imageLoader.getDiskCache();
                    File file = imageLoader.getDiskCache().get(list.get(pager.getCurrentItem()).getBigImg());

                    Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                    String imgHref = list.get(pager.getCurrentItem()).getBigImg();
                    String end = imgHref.substring(imgHref.lastIndexOf("."));
                    File outFile = new File(SqlDataHelper.imagesPath+"/"+list.get(pager.getCurrentItem()).getTitle()+end);
                    if (!new File(SqlDataHelper.imagesPath).exists()){
                        new File(SqlDataHelper.imagesPath).mkdirs();
                    }
                    if (!outFile.exists()) {

                        FileOutputStream outputStream = new FileOutputStream(outFile);
                        FileInputStream inputStream = new FileInputStream(file);
                        byte[] buff = new byte[1024];
                        int count = -1;
                        while ((count = inputStream.read(buff)) >0) {
                            outputStream.write(buff,0,count);
                        }
                        outputStream.flush();
                        outputStream.close();
                        inputStream.close();
                    }
                    handler.sendEmptyMessage(1);
                    handler.sendEmptyMessage(2);

                } catch (Exception e) {
                    e.printStackTrace();
                    handler.sendEmptyMessage(1);
                }
            }
        }.start();
    }

    private void setMySkin() {
        dialog.show();
        new Thread(){
            @Override
            public void run() {
                try {

                    imageLoader.getDiskCache();
                    File file = imageLoader.getDiskCache().get(list.get(pager.getCurrentItem()).getBigImg());

                    Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                    String imgHref = list.get(pager.getCurrentItem()).getBigImg();
                    String end = imgHref.substring(imgHref.lastIndexOf("."));
                    File outFile = new File(getBaseContext().getFilesDir().getAbsolutePath()+"/skin/"+"skin"+end);
                    if (!new File(SqlDataHelper.imagesPath).exists()){
                        new File(SqlDataHelper.imagesPath).mkdirs();
                    }
                    if (!outFile.exists()) {

                        FileOutputStream outputStream = new FileOutputStream(outFile);
                        FileInputStream inputStream = new FileInputStream(file);
                        byte[] buff = new byte[1024];
                        int count = -1;
                        while ((count = inputStream.read(buff)) >0) {
                            outputStream.write(buff,0,count);
                        }
                        outputStream.flush();
                        outputStream.close();
                        inputStream.close();


                    }
                    handler.sendEmptyMessage(1);
                    handler.sendEmptyMessage(3);


                } catch (Exception e) {
                    e.printStackTrace();
                    handler.sendEmptyMessage(1);
                }
            }
        }.start();

    }

    private Handler handler = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            switch (msg.what){
                case 1:
                    if (dialog != null&&dialog.isShowing()){
                        dialog.dismiss();
                    }
                    break;
                case 2:
                    Toast.makeText(getApplicationContext(),"保存图片成功",Toast.LENGTH_SHORT).show();
                    break;
                case 3:

                    recreate();
                    break;
            }
        }
    };
    private static final int WALLPAPERSCREENSSPAN = 1;
    private void setMyWallpaper() {
        dialog.show();

        new Thread(){
            @Override
            public void run() {
                try {

                    imageLoader.getDiskCache();
                    File file = imageLoader.getDiskCache().get(list.get(pager.getCurrentItem()).getBigImg());

                    Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                    WallpaperManager wpm =   WallpaperManager.getInstance(getApplicationContext());
                    Display display = getWindowManager().getDefaultDisplay();
                    final int width = display.getWidth();
                    final int height = display.getHeight();
                    int WALLPAPER_SCREENS_SPAN = 1;
                    if (bitmap.getWidth()/width>=2){
                        WALLPAPER_SCREENS_SPAN =2;
                    }
                    wpm.suggestDesiredDimensions(width * WALLPAPER_SCREENS_SPAN, height);

                    setWallpaper(bitmap);
                    handler.sendEmptyMessage(1);

                } catch (Exception e) {
                    e.printStackTrace();
                    handler.sendEmptyMessage(1);
                }
            }
        }.start();

    }

    @Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putInt(STATE_POSITION, pager.getCurrentItem());
	}

    @Override
    public void onPageScrolled(int i, float v, int i2) {
        Toast.makeText(getApplicationContext(),"onPageScrolled" +"i = "+ i+ "i2 = " + i2,Toast.LENGTH_SHORT).show();
        Log.e("tag","onPageScrolled" +"i = "+ i+ "i2 = " + i2);
    }

    @Override
    public void onPageSelected(int i) {
        Toast.makeText(getApplicationContext(),"onPageSelected" +"i = "+ i,Toast.LENGTH_SHORT).show();
        Log.e("tag","onPageSelected" +"i = "+ i);
    }

    @Override
    public void onPageScrollStateChanged(int i) {
        Toast.makeText(getApplicationContext(),"onPageScrollStateChanged" +"i = "+ i,Toast.LENGTH_SHORT).show();
        Log.e("tag","onPageScrollStateChanged" +"i = "+ i);
    }


    private class ImagePagerAdapter extends PagerAdapter {


        private final ArrayList<ImageDB> list;
        private LayoutInflater inflater;
		private Context mContext;

		ImagePagerAdapter(ArrayList<ImageDB> list,Context context) {
			this.list = list;
			this.mContext=context;
			inflater = getLayoutInflater();
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((View) object);
		}

		@Override
		public void finishUpdate(View container) {
		}

		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public Object instantiateItem(ViewGroup view, int position) {
			View imageLayout = inflater.inflate(R.layout.item_pager_image, view, false);
			
			PhotoView imageView = (PhotoView) imageLayout.findViewById(R.id.image);
            TextView textView = (TextView)imageLayout.findViewById(R.id.jianjie);
            textView.setText(list.get(position).getJianjie()==null||list.get(position).getJianjie().equals("")?list.get(position).getTitle():list.get(position).getJianjie());
			final ProgressBar spinner = (ProgressBar) imageLayout.findViewById(R.id.loading);

			imageLoader.displayImage(list.get(position).getBigImg(), imageView, options, new ImageLoadingListener() {
				@Override
				public void onLoadingStarted(String imageUri, View view) {
					spinner.setVisibility(View.VISIBLE);
				}

				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
					String message = null;
					switch (failReason.getType()) {
						case IO_ERROR:
							message = "Input/Output error";
							break;
						case DECODING_ERROR:
							message = "Image can't be decoded";
							break;
						case NETWORK_DENIED:
							message = "Downloads are denied";
							break;
						case OUT_OF_MEMORY:
							message = "Out Of Memory error";
							break;
						case UNKNOWN:
							message = "Unknown error";
							break;
					}
					Toast.makeText(ImagePagerActivity.this, message, Toast.LENGTH_SHORT).show();

					spinner.setVisibility(View.GONE);
				}

				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					spinner.setVisibility(View.GONE);
				}

                @Override
                public void onLoadingCancelled(String s, View view) {

                }
            });

			((ViewPager) view).addView(imageLayout, 0);
			return imageLayout;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View container) {
		}
	}
}