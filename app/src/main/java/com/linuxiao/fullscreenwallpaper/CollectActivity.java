package com.linuxiao.fullscreenwallpaper;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.MediaRouteButton;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.linuxiao.fullscreenwallpaper.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CollectActivity extends Activity {

    private GridView gridView;
    private ArrayList<ImageDB> db = new ArrayList<ImageDB>();
    protected ImageLoader imageLoader = ImageLoader.getInstance();
    private DisplayImageOptions options;
    private Drawable drawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collect);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        initDatas();
        gridView =(GridView)  findViewById(R.id.gridView);
        gridView.setAdapter(new CollctAdapter(db));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), CollectDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", db);
                bundle.putInt("index", i);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .resetViewBeforeLoading(true)
                .cacheOnDisc(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();
        drawable = getResources().getDrawable(R.drawable.load_default);
    }

    private void initDatas() {
        try {
            File file = new File(SqlDataHelper.imagesPath);
            for (File f : file.listFiles()) {
                ImageDB imageDB = new ImageDB();
                imageDB.setPaht(f.getAbsolutePath());
                db.add(imageDB);
            }
        }catch (Exception e){

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.collect, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }else if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private class CollctAdapter extends BaseAdapter {
        private final List<ImageDB> db;
        private final LayoutInflater inflater;

        public CollctAdapter(List<ImageDB> db) {
            this.db = db;
            inflater = LayoutInflater.from(getApplicationContext());
        }

        @Override
        public int getCount() {
            return db.size();
        }

        @Override
        public Object getItem(int i) {
            return db.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            final ViewHolder holder ;

            if (view == null){
                holder = new ViewHolder();
                view = inflater.inflate(R.layout.item_collect,viewGroup,false);
                holder.imageView = (ImageView) view.findViewById(R.id.imageView);
                holder.textView = (TextView) view.findViewById(R.id.textView);
                holder.pbLoad = (ProgressBar) view.findViewById(R.id.pb_load);

                view.setTag(holder);
            }else {
                holder = (ViewHolder) view.getTag();
            }
            ImageDB imageDB =db.get(i);
            File file = new File(imageDB.getPaht());
            holder.textView.setText(file.getName());
            imageLoader.displayImage("file://"+imageDB.getPaht(),holder.imageView,options,new ImageLoadingListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    holder.imageView.setImageDrawable(drawable);
                    holder.pbLoad.setVisibility(View.VISIBLE);

                }

                @Override
                public void onLoadingFailed(String imageUri, View view,
                                            FailReason failReason) {
                    String message = null;
                    switch (failReason.getType()) {
                        case IO_ERROR:
                            message = "Input/Output error";
                            break;
                        case DECODING_ERROR:
                            message = "can not be decoding";
                            break;
                        case NETWORK_DENIED:
                            message = "Downloads are denied";
                            break;
                        case OUT_OF_MEMORY:
                            message = "内存不足";
                            Toast.makeText(CollectActivity.this, message, Toast.LENGTH_SHORT)
                                    .show();
                            break;
                        case UNKNOWN:
                            message = "Unknown error";
                            Toast.makeText(CollectActivity.this, message, Toast.LENGTH_SHORT)
                                    .show();
                            break;
                    }
                    holder.pbLoad.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view,
                                              Bitmap loadedImage) {
                    holder.pbLoad.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingCancelled(String paramString,
                                               View paramView) {
                }
            });
            return view;
        }

        private class ViewHolder {
            ImageView imageView;
            TextView textView;

            public ProgressBar pbLoad;
        }
    }
}
