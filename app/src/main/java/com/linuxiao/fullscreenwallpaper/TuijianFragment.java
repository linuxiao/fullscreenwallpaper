package com.linuxiao.fullscreenwallpaper;


import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.huewu.pla.lib.MultiColumnListView;
import com.huewu.pla.lib.MultiColumnPullToRefreshListView;
import com.huewu.pla.lib.internal.PLA_AdapterView;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TuijianFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TuijianFragment extends Fragment {

    private ArrayList<ImageDB> list = new ArrayList<ImageDB>();
    private MultiColumnPullToRefreshListView multiColumnListView;
    WaterfallAdapter adapter;
    private  int curPage = 0;
    public  String fbl  = "x";

    // TODO: Rename and change types and number of parameters
    public static TuijianFragment newInstance(String fbl,int page) {
        TuijianFragment fragment = new TuijianFragment();
        fragment.fbl = fbl;
        fragment.curPage = page;
        return fragment;
    }
    public static TuijianFragment newInstance(ArrayList<ImageDB> list) {
        TuijianFragment fragment = new TuijianFragment();
        fragment.list = list;
        return fragment;
    }

    public TuijianFragment() {
        // Required empty public constructor
    }

    private Handler handler = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            switch (msg.what) {
                case 2:
                    adapter.setList(list);

                    adapter.notifyDataSetChanged();
                    multiColumnListView.onRefreshComplete();
                    break;
                case 1:
                    adapter.setList(list);

                    adapter.notifyDataSetChanged();
                    multiColumnListView.onLoadMoreComplete();
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tuijian, container, false);
        findViews(rootView);
        return rootView;
    }

    private void findViews(View rootView) {
        multiColumnListView = (MultiColumnPullToRefreshListView) rootView.findViewById(R.id.view);




        adapter = new WaterfallAdapter(list, getActivity().getApplicationContext());
        multiColumnListView.setAdapter(adapter);
        multiColumnListView.setOnItemClickListener(new PLA_AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(PLA_AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity().getApplicationContext(), ImagePagerActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", list);
                bundle.putInt("index", position);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        final SqlDataHelper helper = new SqlDataHelper(getActivity().getApplicationContext());
        helper.open();
        multiColumnListView.setOnRefreshListener(new MultiColumnPullToRefreshListView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // TODO Auto-generated method stub
                //下拉刷新要做的事
                new Thread() {
                    @Override
                    public void run() {

                        curPage =(int) Math.random()*100;

                        list = helper.getImages(fbl, 20, curPage * 20);
                        handler.sendEmptyMessage(2);
                    }
                }.start();
                //刷新完成后记得调用这个
                // multiColumnListView.onRefreshComplete();
            }
        });
        multiColumnListView.setOnLoadMoreListener(new MultiColumnListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
               // Toast.makeText(getActivity().getApplicationContext(), "加载更多", Toast.LENGTH_LONG).show();
                //下拉刷新要做的事
                new Thread() {
                    @Override
                    public void run() {
                        curPage++;
                        list.addAll(helper.getImages(fbl, 20, curPage * 20));
                        handler.sendEmptyMessage(1);
                    }
                }.start();

            }
        });
    }

    private String baseUrl = "http://sj.enterdesk.com/";

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final SqlDataHelper helper = new SqlDataHelper(getActivity().getApplicationContext());
        helper.open();
        multiColumnListView.setRefreshing();

        new Thread() {
            @Override
            public void run() {
                list = helper.getImages(fbl, 20, curPage * 20);
                if (list==null || list.size() ==0 ){
                    fbl = "1080x1920";
                    list = helper.getImages("1080x1920",20,curPage*20);
                }
                handler.sendEmptyMessage(2);
            }
        }.start();  


    }
}
