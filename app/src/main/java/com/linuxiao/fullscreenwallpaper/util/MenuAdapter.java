package com.linuxiao.fullscreenwallpaper.util;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.linuxiao.fullscreenwallpaper.R;

import java.util.ArrayList;

public class MenuAdapter extends BaseAdapter {
    private final ArrayList<DBMenu> menus;
    private final Context mContext;
    LayoutInflater inflater;
    private int curIndex = 0;

    public void setCurIndex(int curIndex) {
        this.curIndex = curIndex;
    }

    public MenuAdapter(ArrayList<DBMenu> menus, Context mContext) {
        this.menus = menus;
        this.mContext = mContext;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return menus.size();
    }


    @Override
    public Object getItem(int i) {
        return menus.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    class ViewHoder {
        ImageView imageView;
        TextView textView;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHoder hoder = null;
        if (view == null) {
            hoder = new ViewHoder();
            view = inflater.inflate(R.layout.item_icon_text, viewGroup, false);
            hoder.imageView = (ImageView) view.findViewById(R.id.imageView);
            hoder.textView = (TextView) view.findViewById(R.id.textView);
            view.setTag(hoder);
        } else {
            hoder = (ViewHoder) view.getTag();
        }
        DBMenu menu = menus.get(i);
        hoder.textView.setText(menu.getTitle());
        if (i == curIndex) {
            hoder.textView.setTextColor(Color.BLUE);
            view.setBackgroundColor(mContext.getResources().getColor(R.color.halfBlack));
        } else {
            hoder.textView.setTextColor(Color.WHITE);
            view.setBackgroundColor(Color.TRANSPARENT);
        }
//            hoder.imageView.set
        return view;
    }

}