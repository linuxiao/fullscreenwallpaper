package com.linuxiao.fullscreenwallpaper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;

/**
 * Created by xiongxiaobin on 14-7-20.
 */
public class TuiJianActivity extends FragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tuijian_activity);
        ((LXApplication)getApplication()).addActivity(this);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Intent intent = getIntent();
        String fbl = intent.getStringExtra("fbl");
        int page = intent.getIntExtra("page",0);
        fragmentManager.beginTransaction()
                .replace(R.id.container, TuijianFragment.newInstance(fbl,page))
                .commit();
        getActionBar().setDisplayHomeAsUpEnabled(true);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
        {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
