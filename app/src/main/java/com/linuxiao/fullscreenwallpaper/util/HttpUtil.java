package com.linuxiao.fullscreenwallpaper.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.BinaryHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class HttpUtil {
    private static final String TAG = "HttpUtil";
    private static final AsyncHttpClient client = new AsyncHttpClient(); // 实例话对象

    static {
        client.setTimeout(5000); // 设置链接超时，如果不设置，默认为10s

        // client.setCookieStore(cookieStore);

    }

    public static void get(String urlString, AsyncHttpResponseHandler res) // 用一个完整url获取一个string对象
    {
        client.get(urlString, res);
    }


    public static boolean getInternetState(Context context) {
        // 获取手机所有连接管理对象（包括对wi-fi,net等连接的管理）
        try {
            ConnectivityManager connectivity = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                // 获取网络连接管理的对象
                NetworkInfo info = connectivity.getActiveNetworkInfo();
                if (info != null && info.isConnected()) {
                    // 判断当前网络是否已经连接
                    if (info.getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
            Log.v("error", e.toString());
        }
        return false;
    }

    public static void get(String urlString, RequestParams params,
                           AsyncHttpResponseHandler res) // url里面带参数
    {
        client.get(urlString, params, res);
    }

    /**
     * 　　* post 请求 　　* 　　* @param url 　　* API 地址 　　* @param params 　　* 请求的参数 　　* @param
     * handler 　　* 数据加载句柄对象
     */
    public static void post(String url, RequestParams params,
                            AsyncHttpResponseHandler handler) {
        System.out.println("进入post");
        client.post(url, params, handler);
    }

    public static void get(String urlString, JsonHttpResponseHandler res) // 不带参数，获取json对象或者数组
    {
        client.get(urlString, res);
    }

    public static void get(String urlString, RequestParams params,
                           JsonHttpResponseHandler res) // 带参数，获取json对象或者数组
    {
        client.get(urlString, params, res);
    }

    public static void get(String uString, BinaryHttpResponseHandler bHandler) // 下载数据使用，会返回byte数据
    {
        client.get(uString, bHandler);
    }

    public static AsyncHttpClient getClient() {
        return client;
    }
}