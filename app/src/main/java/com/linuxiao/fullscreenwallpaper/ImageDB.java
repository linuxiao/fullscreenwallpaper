package com.linuxiao.fullscreenwallpaper;

import java.io.Serializable;

/**
 * Created by xiongxiaobin on 14-7-13.
 */
public class ImageDB implements Serializable {
    private String href;
    private String img;
    private String title;
    private String jianjie;
    private String bigImg;
    private String page;
    private String fbl;
    private String paht;

    public String getTitle() {
        return title;
    }

    public String getHref() {
        return href;
    }

    public String getImg() {
        return img;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setBigImg(String bigImg) {
        this.bigImg = bigImg;
    }

    public String getBigImg() {
        return bigImg;
    }

    public void setJianjie(String jianjie) {
        this.jianjie = jianjie;
    }

    public String getJianjie() {
        return jianjie;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public void setFbl(String fbl) {
        this.fbl = fbl;
    }

    public void setPaht(String paht) {
        this.paht = paht;
    }

    public String getFbl() {
        return fbl;
    }

    public String getPage() {
        return page;
    }

    public String getPaht() {
        return paht;
    }
}
