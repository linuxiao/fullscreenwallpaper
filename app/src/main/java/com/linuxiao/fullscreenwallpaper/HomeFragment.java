package com.linuxiao.fullscreenwallpaper;


import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {

    private static HomeFragment fragment;
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance() {
        if (fragment == null){
            fragment = new HomeFragment();
        }


        return fragment;
    }

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        findView(rootView);
        Log.e("tag","HomeFragment onCreateView");
        return  rootView;
    }

    private void findView(View rootView) {
        rootView.findViewById(R.id.history).setOnClickListener(this);
        rootView.findViewById(R.id.file).setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.history:
                Intent intent = new Intent(getActivity(),CollectActivity.class);
                getActivity().startActivity(intent);
                break;
            case R.id.file:
                Intent intent1 = new Intent(getActivity(),ImageViewerActivity.class);
                File file = Environment.getExternalStorageDirectory();
                Bundle bundle = new Bundle();
                bundle.putSerializable(ImageViewerActivity.Extra.IMAGES,file.list());

                getActivity().startActivity(intent1);
                break;
        }
    }
}
