package com.linuxiao.fullscreenwallpaper;

import android.app.Activity;
import android.app.Application;
import android.graphics.Bitmap;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by xiongxiaobin on 14-7-23.
 */
public class LXApplication extends Application {
    private static List<Activity> activityList = new LinkedList<Activity>();
    private static LXApplication instance;
    public LXApplication(){}
    public  static Application getInstance(){
        if (instance == null){
            return new LXApplication();
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //初始化图片加载库
        File cacheDir = new File(SqlDataHelper.homePaht+"cache");
        if (!cacheDir.exists()){
            cacheDir.mkdirs();
        }
        DisplayImageOptions defaultOptions =
                new DisplayImageOptions.Builder()
                        .cacheOnDisc(true)//图片存本地

                        .cacheInMemory(true)
                        .displayer(new FadeInBitmapDisplayer(50))
                        .bitmapConfig(Bitmap.Config.RGB_565)
                        .imageScaleType(ImageScaleType.EXACTLY) // default

                        .build();
        ImageLoaderConfiguration config =
                new ImageLoaderConfiguration.Builder(getApplicationContext())
                        .memoryCache(new UsingFreqLimitedMemoryCache(16 * 1024 * 1024))
                        .diskCache(new UnlimitedDiscCache(cacheDir))
                        .defaultDisplayImageOptions(defaultOptions).build();
        ImageLoader.getInstance().init(config);
    }
    //Activity加入到List中
    public  void addActivity(Activity activity){
        activityList.add(activity);
    }
    //遍历每个Activity退出
    public  void exit(){
        for(Activity activity:activityList){
            activity.finish();
        }
        System.exit(0);
    }
    public  void setSkin(){
        for (Activity activity:activityList){
            activity.recreate();
        }
    }

}
