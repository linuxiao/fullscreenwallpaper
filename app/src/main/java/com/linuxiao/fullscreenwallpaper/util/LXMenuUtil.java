package com.linuxiao.fullscreenwallpaper.util;

import android.content.Context;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by xiongxiaobin on 14-7-9.
 */
public class LXMenuUtil {

    public static ArrayList<DBMenu> getMenuLists(Context mContext) {
        ArrayList<DBMenu> dbMenus = new ArrayList<DBMenu>();

        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(mContext.getAssets().open("menus.xml"), "UTF-8");
            int eventType = parser.getEventType();
            DBMenu curMenu = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {

                    case XmlPullParser.START_TAG:

                        String name = parser.getName();
                        if (name.equalsIgnoreCase("menu")) {
                            curMenu = new DBMenu();
                        } else if (name.equalsIgnoreCase("title")) {
                            String tmp = parser.nextText().toString();
                            curMenu.setTitle(tmp);
                        } else if (name.equalsIgnoreCase("image")) {
                            curMenu.setIconNomal(parser.getAttributeValue(null, "nomal"));
                            curMenu.setIconSelected(parser.getAttributeValue(null, "selsected"));
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        if (parser.getName().equalsIgnoreCase("menu") && curMenu != null) {
                            dbMenus.add(curMenu);
                            curMenu = null;
                        }
                        break;
                }
                eventType = parser.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return dbMenus;
    }
}
