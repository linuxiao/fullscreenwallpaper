package com.linuxiao.fullscreenwallpaper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Administrator on 2014/7/15.
 */
public class SqlDataHelper extends SQLiteOpenHelper {
    private SQLiteDatabase db;
    private Context context;
    private String path = Environment.getExternalStorageDirectory().getAbsolutePath()+"/fullscreenwallpaper/db/fullscreenwallpaper";
    public static String imagesPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/fullscreenwallpaper/images";
    public static String homePaht = Environment.getExternalStorageDirectory().getAbsolutePath()+"/fullscreenwallpaper/";
    public  static  String [] tmps = new String[] {"1080x1920","800x1280","720x1280","800x960","800x960","768x1280","640x1136","640x960","540x960","480x800"};

    public SqlDataHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context;
    }

    public SqlDataHelper(Context context, String name) {
        super(context, name, null, 1);
        this.context = context;
    }

    public SqlDataHelper(Context context) {
        super(context, "fullscreenwallpaper", null, 1);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        File file = new File(path);
        if (!file.exists()) {
//            String sql = "create table images (_id INTEGER not null  PRIMARY KEY autoincrement,href text,title text,img text,jianjie text,bigImg text,paht text, page int,fbl text )";
//            sqLiteDatabase.execSQL(sql);
            copyDataBase();
        }

    }

    public void copyDataBase() {
        File outFile = new File(path);
        if (outFile.exists()){
            return;
        }
        if (!outFile.getParentFile().exists()){
            outFile.getParentFile().mkdirs();
        }
        try {
            FileOutputStream os = new FileOutputStream(outFile);
            InputStream is= context.getAssets().open("fullscreenwallpaper");
            int count =-1;
            byte[] buff = new byte[1024];
            while ((count = is.read(buff))>0){
                os.write(buff,0,count);
            }
            os.flush();
            os.close();
            is.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void open() {
        if (db == null) {
            copyDataBase();
            db = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READWRITE);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

    }

    public int UpdateImage() {
        int i = 0;
        //http://sj.enterdesk.com/1080x1920/1.html
        // String baseUrl = "http://sj.enterdesk.com/1080x1920/";
        HttpImages httpImages = new HttpImages();
        for (int m = 0; m < tmps.length; m++) {
            for (int j = 0; j < 20; j++) {
                Log.e("tag", "第 " + j);

                ArrayList<ImageDB> list = httpImages.getImageList(HttpImages.baseUrl,tmps[m]+"/", j + "");
                for (int k = 0; k < list.size(); k++) {
                    ImageDB imageDB = list.get(k);
                    ContentValues initialValues = new ContentValues();
                    initialValues.put("href", imageDB.getHref());
                    initialValues.put("title", imageDB.getTitle());
                    initialValues.put("img", imageDB.getImg());
                    initialValues.put("jianjie", imageDB.getJianjie());
                    initialValues.put("bigImg", imageDB.getBigImg());
                    initialValues.put("page", j);
                    initialValues.put("fbl", tmps[m]);
                    db.replace("images", null, initialValues);
                }
            }
        }



        return i;
    }
    public void update22mm(){
        HttpImages httpImages = new HttpImages();
        String[] urls = new String[]{"mm/qingliang/","mm/bagua/","mm/jingyan/","mm/suren/","mm/picbest/"};
        for (int m = 0; m < urls.length; m++) {
            for (int j = 1; j < 120; j++) {
                Log.e("tag", "第 " + j);

                ArrayList<ImageDB> list = httpImages.getImageListFrom22mm("http://www.22mm.cc/"+urls[m],"all",j+"");
                for (int k = 0; k < list.size(); k++) {
                    ImageDB imageDB = list.get(k);
                    ContentValues initialValues = new ContentValues();
                    initialValues.put("href", imageDB.getHref());
                    initialValues.put("title", imageDB.getTitle());
                    initialValues.put("img", imageDB.getImg());
                    initialValues.put("jianjie", imageDB.getJianjie());
                    initialValues.put("bigImg", imageDB.getBigImg());
                    initialValues.put("page", j);
                    initialValues.put("fbl", "x");
                    db.replace("images", null, initialValues);
                }
            }
        }

    }

    /**
     * 查询图片
     *
     * @param fenbianlv 分辨率
     * @param offset    每页数量
     * @param curIndex  当前位置
     * @return
     */
    public ArrayList<ImageDB> getImages(String fenbianlv, int offset, int curIndex) {
        ArrayList<ImageDB> list = new ArrayList<ImageDB>();
        String str = "select * from images where  fbl like ? order by title desc limit ? offset ?";
        open();
        Cursor cursor = db.rawQuery(str, new String[]{"%" + fenbianlv + "%", offset + "", curIndex + ""});
        while (cursor.moveToNext()) {
            ImageDB imageDB = new ImageDB();
            imageDB.setBigImg(cursor.getString(cursor.getColumnIndex("bigImg")));
            imageDB.setFbl(cursor.getString(cursor.getColumnIndex("fbl")));
            imageDB.setHref(cursor.getString(cursor.getColumnIndex("href")));
            imageDB.setTitle(cursor.getString(cursor.getColumnIndex("title")));
            imageDB.setJianjie(cursor.getString(cursor.getColumnIndex("jianjie")));
            imageDB.setPage(cursor.getString(cursor.getColumnIndex("page")));
            imageDB.setPaht(cursor.getColumnName(cursor.getColumnIndex("paht")));
            imageDB.setImg(cursor.getColumnName(cursor.getColumnIndex("img")));
            list.add(imageDB);
        }
        return list;
    }
}
