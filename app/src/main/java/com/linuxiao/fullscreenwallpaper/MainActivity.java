package com.linuxiao.fullscreenwallpaper;

import android.app.ActionBar;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.linuxiao.fullscreenwallpaper.util.LXMenuUtil;


public class MainActivity extends FragmentActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private Fragment[] fragments = new Fragment[]{MainFrament.newInstance(),FuliFragment.newInstance(),SettingFragment.newInstance(),AboutFragment.newInstance()};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ((LXApplication)getApplication()).addActivity(this);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);

        mTitle = getTitle();
        for (Fragment f : fragments){
            getSupportFragmentManager().beginTransaction().add(R.id.container,f).commit();
        }
        getSupportFragmentManager().beginTransaction().show(fragments[0]).commit();
//        new Thread(){
//            @Override
//            public void run() {
//                SqlDataHelper helper = new SqlDataHelper(getApplicationContext());
//                helper.open();
//                helper.update22mm();
//
//                Log.i("tag", "啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊");
//            }
//        }.start();
//        new Thread(){
//            @Override
//            public void run() {
//                SqlDataHelper helper = new SqlDataHelper(getApplicationContext());
//                helper.open();
//                helper.UpdateImage();
//
//                Log.i("tag", "啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊");
//            }
//        }.start();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
//        onSectionAttached(position);
        // update the main content by replacing fragments
        Log.e("tag", "选择了" + position);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
//        if (position == 0){
//            if (fragments[position].isAdded()){
//                if (fragments[position].isHidden()){
//                    transaction.show(fragments[position]);
//                }
//            }else {
//                transaction.replace(R.id.container,fragments[position]);
//            }
//        }else {
//            transaction.replace(R.id.container,fragments[position]);
//        }
        for (int i = 0; i < fragments.length; i++) {

            if (i== position){
                transaction.show(fragments[i]);
            }else {
                transaction.hide(fragments[i]);
            }
        }

        transaction.commit();
//        fragmentManager.beginTransaction()
//                .replace(R.id.container, fragments[position])
//                .commit();
        onSectionAttached(position);
        restoreActionBar();

    }

    public void onSectionAttached(int number) {

        mTitle = LXMenuUtil.getMenuLists(getApplicationContext()).get(number).getTitle();
//        switch (number) {
//            case 1:
//                mTitle = getString(R.string.title_section1);
//                break;
//            case 2:
//                mTitle = getString(R.string.title_section2);
//                break;
//            case 3:
//                mTitle = getString(R.string.title_section3);
//                break;
//        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
