package com.linuxiao.fullscreenwallpaper;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;


/**
 * Created by xiongxiaobin on 14-7-13.
 */
public class HttpImages {
    public static String baseUrl = "http://sj.enterdesk.com/";

    public ArrayList<ImageDB> getImageList(String strFenBianLv) {
        ArrayList<ImageDB> imageDBs = null;
        try {
            imageDBs = new ArrayList<ImageDB>();
            org.jsoup.nodes.Document doc = Jsoup.connect(baseUrl + strFenBianLv).get();
            Elements element = doc.getElementsByClass("sbody");
            for (int i = 0; i < element.size(); i++) {
                Element e = element.get(i);
                Elements childE = e.getElementsByTag("img");
                if (childE.size() != 0) {
                    ImageDB db = new ImageDB();
                    db.setImg(childE.attr("src"));
                    db.setTitle(childE.attr("title"));
                    db.setHref(childE.get(0).parent().getElementsByTag("a").get(0).attr("href"));
                    //获取简介和大图Url
                    org.jsoup.nodes.Document tmpDoc = Jsoup.connect(db.getHref()).get();
                    Elements tmpElement = tmpDoc.getElementsByClass("jianjie");
                    db.setJianjie(tmpElement.get(0).getElementsByTag("p").get(0).ownText());
                    String str = tmpDoc.getElementsByClass("arc_push3").get(0).getElementsByTag("a").get(0).attr("onclick");
                    str = str.substring(str.lastIndexOf(",") + 2, str.lastIndexOf("'"));
                    db.setBigImg(str);
                    imageDBs.add(db);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageDBs;
    }

    public ArrayList<ImageDB> getImageList(String baseUrl,String strFenBianLv, String page) {
        ArrayList<ImageDB> imageDBs = null;
        try {
            imageDBs = new ArrayList<ImageDB>();
            org.jsoup.nodes.Document doc = Jsoup.connect(baseUrl + strFenBianLv + page + ".html").get();
            Log.e("tag", baseUrl + strFenBianLv + page + ".html");
            Elements element = doc.getElementsByClass("sbody");
            for (int i = 0; i < element.size(); i++) {
                Element e = element.get(i);
                Elements childE = e.getElementsByTag("img");
                if (childE.size() != 0) {
                    ImageDB db = new ImageDB();
                    db.setImg(childE.attr("src"));
                    db.setTitle(childE.attr("title"));
                    db.setHref(childE.get(0).parent().getElementsByTag("a").get(0).attr("href"));
                    //获取简介和大图Url
                    org.jsoup.nodes.Document tmpDoc = Jsoup.connect(db.getHref()).get();
                    Elements tmpElement = tmpDoc.getElementsByClass("jianjie");
                    db.setJianjie(tmpElement.get(0).getElementsByTag("p").get(0).ownText());
                    String str = tmpDoc.getElementsByClass("arc_push3").get(0).getElementsByTag("a").get(0).attr("onclick");
                    str = str.substring(str.lastIndexOf(",") + 2, str.lastIndexOf("'"));
                    db.setBigImg(str);
                    imageDBs.add(db);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageDBs;
    }
    public ArrayList<ImageDB> getImageListFrom22mm(String baseUrl,String strFenBianLv ,String page) {
        ArrayList<ImageDB> imageDBs = null;
        try {
            imageDBs = new ArrayList<ImageDB>();
            String url = baseUrl +"index_"  + page + ".html";
            if (page.equals("0")){
                url=baseUrl;
            }
            org.jsoup.nodes.Document doc = Jsoup.connect(url).get();


            Elements element = doc.getElementsByTag("li");
            for (int i = 0; i < element.size(); i++) {
                Element e = element.get(i);
                Elements childE = e.getElementsByTag("a");
                if (childE.size() != 0) {
                    ImageDB db = new ImageDB();
                    db.setHref(childE.attr("href"));

                    db.setTitle(childE.attr("title"));
                    db.setImg(childE.get(0).getElementsByTag("img").attr("src"));
                    //获取简介和大图Url
                    org.jsoup.nodes.Document tmpDoc = Jsoup.connect("http://www.22mm.cc/"+db.getHref()).get();
                    String a = "big";
                    String b = "pic";
                   String str = tmpDoc.getElementById("box-inner").getElementsByTag("script").get(1).toString();
                    str = str.replace(a,b);
                    str = str.replace("arrayImg[0]=","").trim();
                    str = str.replace("arrayImg[1]=","").trim();
                    str = str.replace("arrayImg[2]=","").trim();
                    str = str.replace("arrayImg[3]=","").trim();
                    str = str.replace("arrayImg[4]=","").trim();
                    str = str.replace("<script>var arrayImg=new Array()","").trim();
                    str = str.replace("getImgString()</script>","").trim();
                    str = str.replace("\"","");
                    String[] strArr = str.split(";");
                    for (int j = 0; j <strArr.length ; j++) {
                        if (!strArr[j].equals("")) {
                            ImageDB tmpdb = new ImageDB();
                            tmpdb.setTitle(db.getTitle());
                            tmpdb.setHref(db.getHref());
                            tmpdb.setBigImg(strArr[j].replace(";", "").trim());
                            Log.e("tag","url: "+ tmpdb.getBigImg());
                            tmpdb.setImg(db.getImg().trim());
                            imageDBs.add(tmpdb);
                        }
                    }
//                    db.setBigImg(str.);
//                    imageDBs.add(db);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageDBs;
    }
}
