package com.linuxiao.fullscreenwallpaper.util;

/**
 * Created by xiongxiaobin on 14-7-9.
 */
public class DBMenu {
    private String title;
    private String iconSelected;
    private String iconNomal;

    public String getIconNomal() {
        return iconNomal;
    }

    public String getIconSelected() {
        return iconSelected;
    }

    public String getTitle() {
        return title;
    }

    public void setIconNomal(String iconNomal) {
        this.iconNomal = iconNomal;
    }

    public void setIconSelected(String iconSelected) {
        this.iconSelected = iconSelected;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
